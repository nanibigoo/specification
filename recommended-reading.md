## 推荐阅读

### 文档资料

 - **必须** 熟记 [Laravel 5.5 官方文档](https://d.laravel-china.org/docs/5.5)，查阅时能快速定位，5 遍以上；
 - **必须** 熟记 [Laravel 5.5 API 文档](https://laravel.com/api/5.5/) 的类结构，查阅时能快速定位；
 - **必须** 熟记所有 [PSR 通过的标准](https://github.com/summerblue/psr.phphub.org)；
 - PSR 目前还未通过的标准，也要 应该 知晓 [http://www.php-fig.org/psr/#draft](http://www.php-fig.org/psr/#draft)
 - **应该** 熟悉 [PHP 最佳实践](http://laravel-china.github.io/php-the-right-way/)
 - **应该** 了解 [『Rails 信条』](https://laravel-china.org/articles/5232/every-laravel-engineer-should-read-the-rails-credo)

### 教程

 如果你是新手，想从零开始学习规范化编程，请查阅我的两本书：

 - [《Laravel 入门教程 - 从零到部署上线》](https://laravel-china.org/topics/3383/laravel-the-first-chinese-new-book-laravel-tutorial)
 - [《Laravel 进阶课程 - 从零开始构建论坛系统》](https://laravel-china.org/topics/6592/laravel-tutorial-series-book-second-web-developer-combat-advanced-began-to-build-the-forum-system-from-zero)