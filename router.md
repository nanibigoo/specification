## 路由

### 路由闭包

**绝不** 在路由配置文件里书写『闭包路由』或者其他业务逻辑代码，因为一旦使用将无法使用 路由缓存 。

路由器要保持干净整洁，**绝不** 放置除路由配置以外的其他程序逻辑。

### 单数 or 复数？

资源路由路由 URI 必须 使用复数形式，如：

 - ``/photos/create``
 - ``/photos/{photo}``
 
 
错误的例子如：

 - ``/photo/create``
 - ``/photo/{photo}``


 
### 路由命名
 
 所有路由都 **必须** 使用 ``name`` 方法进行命名
 
 **必须** 使用『请求方式』_『资源地址』作为命名规范，如下的 users_follow，资源地址的值是 users_follow：
 
 ```angularjs
Route::post('users/follow', 'UsersController@follow')->name('post_users_follow');
```